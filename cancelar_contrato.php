<?php

require("conexion.php");
require("libreria/funciones.php");

     cancelarCont($_GET['IdContrato']);             //pones como cancelado

     $datosContrato=get_datos_contrato($_GET['IdContrato']);   //Obtienes todos las datos del contrato

   
crear_mensaje('El contrato' .' '. $datosContrato['IdContrato'] .' ' . 'con fechas de inicio y fin' .' '. $datosContrato['FechaHoraInicio'] . ' '.'------ ' . $datosContrato['FechaHoraFin'] .' '.'finalmente ha sido cancelado por el cuidador, es probable que hay tenido algún imprevisto de última hora, intenta nuevos contactos. ¡Muchas gracias!' , $datosContrato['IdUsu'], $_GET['IdContrato'] );       //Creas el aviso en base datos



    header('Content-Type: application/json');
    echo json_encode(['ok'=> true, 'id'=>$_GET['IdContrato']]);

?>