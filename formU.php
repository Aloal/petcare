
 <?php
             
        require("conexion.php");
        require("libreria/validacionesRegU.php");
        require ("libreria/regbdUsuario.php");
        require ("libreria/funciones.php");


        if (isset($_POST['boton'])) {


           $errores=validarUsuario();
           

             if(empty($errores)) {
                
                 registroUsuario();
                   echo '
                  <script>
                    alert("¡Enhorabuena! ya eres Usuario Registrado en Pet CARE");
                    window.location="areaU.php";
                 </script>
                    ';
                 
                 die();

             }
        }

         $zonas=getZona();
          
      ?>




<!DOCTYPE html>
<html lang="es">
<head>



  <meta charset="utf-8">

     
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

     <link rel="stylesheet" href="css/estilos.css" />
   

  <style type="text/css">
    

aside p{
   color: #009999;
   margin-left: 12px;
   margin-top: 70px;
   font-size: 16px;
 }

 .form {
  width: 100%;
  max-width: 600px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  margin-top: 15px;
  margin-bottom: 15px;
}

#cuidador{
  position: center;
  margin-top: 40px;
  margin-bottom: 15px;
}

.btn{
    margin-bottom: 5px;
    
 }

section{
  
  margin-bottom: 25px;
  margin-left: 25px;
}


  </style>

   </head>

  <body>


  
 <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
              <a class="nav-link" href="index.php">Inicio</a>
           </li>

           <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ayuda</a>
                 <div class="dropdown-menu">
                     <a class="dropdown-item" href="ayudaUsu.php">Ayuda Usuarios</a>
                     
                </div>
          </li>             

          <li class="nav-item ml-sm-4">
            <a href="logU.php"  class="btn btn-success">Mi Área</a>
                    
          </li>

      
         </ul>
       </div>
       
     </nav>


 
 
      <section class="main row pt-2 mr-1" >


          <article class="col-xs-12 col-sm-11 col-md-11 col-lg-9 ml-2">

           <h4 style="margin-left: 12px; margin-bottom: 25px; margin-top: 20px;">Regístrate como Usuario con el siguiente formulario y ¡Listo para contactar con cuidadores!</h4>



           <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                     
                <fieldset>
              
               <div class="form-row">
                <div class="form-group col-md-4">
                 <label for="inputUsuario">*Usuario</label>
                  <input type="text" class="form-control" name="nom" required="required" id="inputUsuario" placeholder="Nick">
               </div>
               <div class="form-group col-md-3">
                 <label for="inputPass">*Contraseña</label>
                 <input type="text" class="form-control" id="inputPass" required="required" name="cont" placeholder="5-8 caracteres">
               </div>
                
               </div>

             <div class="form-row">
               <div class="form-group col-md-3">
                <label for="inputNombre">*Nombre</label>
                  <input type="text" class="form-control" id="inputNombre"  name="nombre" placeholder="Nombre y apellidos">
               </div>
               <div class="form-group col-md-4">
                <label for="inputDir">*Dirección</label>
                 <input type="text" class="form-control" name="dir" id="inputDir">
               </div>
                <div class="form-group col-md-3">
                <label for="inputDni">*DNI</label>
                 <input type="text" class="form-control" name="dni" id="inputDni" placeholder="9 caracteres">
               </div>

            </div>


             <div class="form-row">
               <div class="form-group col-md-3">
                <label for="inputEmail">*Correo electrónico</label>
                  <input type="text" class="form-control" id="inputEmail"  name="correo" placeholder="nombre@dominio.extension">
               </div>
               <div class="form-group col-md-3">
                <label for="inputTel">*Teléfono</label>
                 <input type="text" class="form-control" name="tlf" id="inputTel" placeholder="9 digitos">
               </div>
                <div class="form-group col-md-4">
                <label for="inputIban">*IBAN</label>
                 <input type="text" class="form-control" name="iban" id="inputIban" placeholder="ES + 22 dígitos">
               </div>

            </div>


             <div class="form-row" style="margin-top: 12px;">
               <div class="form-group col-md-6">
                 <label for="inputZona">*Zona en la que buscas cuidadores</label></br>
                  <select name="zona" >
                     <option value="">Seleccione zona:</option>

                      <?php 


                      foreach ($zonas as $indice => $registro) { 

                         if($registro['IdZona'] == $usuario['IdZona']){

                           echo "<option selected value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";
                                            

                        }else{
                         echo "<option value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";
                        }
                       
                                              
                      }
                     ?>

                   </select>

                </div>
                   
                     <div class="form-group col-md-4" style="margin-top: 12px;">
                      <input type="checkbox" id="val" value="val" required="required"> <label for="cbox2"> <a href="politica.php" style="text-decoration: none">Acepto las condiciones del servicio</a></label>

                     </div>
            
               </div>

                  
           </br>
                                     
              
            </fieldset>



             <div><input type="submit" name="boton"  value="Registrar" class="btn btn-success"  style="margin-left: 10px;">
               <input type="reset" value="Borrar" class="btn btn-secondary" style="margin-left: 10px;">
             </div>


               <?php

               if(isset($errores)){
                 
                foreach ($errores as $error) {
                  
                   echo '<div style= "color:#ff0000;">'. $error . '</div>';
                }
               }

             ?>
             
        </form>

       
           </br>

             <div class="btn" style="colorfr: #B266FF; margin-bottom: 8px; margin-left: 10px;">
              <a href="javascript: history.go(-1)"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver</strong></a></div>
     

       </article>

       <aside class=" col-lg-2  d-none d-lg-block">
           <p><strong>Una vez estés dado de alta, podrás contactar con el cuidador que desees.Ya son mas de 2.000 personas en toda Asturias las que utilizan PET CARE</strong> </p>

             <img id="cuidador" src="imagenes_video/usuario.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="200" height="200">     
         
              
        </aside>

   </section>

 

          
     <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square fa-xl " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram fa-xl " ></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square fa-xl" ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>


   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>