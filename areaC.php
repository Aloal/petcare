<?php

  require("conexion.php");
  require("libreria/funciones.php");
  require ("libreria/borrarCuidador.php");

  session_start();


   if(!isset($_SESSION['cuidador'])){
     header("location: logC.php");
      die();
     }

      $cuidados=getCuidado();
      $mascotas=getMascotas();
      $zonas=getZona();


    

     $cuidador=getCuidador_por_nombre($_SESSION['cuidador']);

     $resulcontratos=tablaContratos($_SESSION['idcui']);



        if (isset($_POST['actualizar'])) {
        
        update_cuidador($cuidador['IdCui']);

       $cuidador=getCuidador_por_nombre($_SESSION['cuidador']);

    }




  
       if (isset($_POST['guardarAnun'])) {
        
        updateAnun($cuidador['IdCui']);

          echo '
                  <script>
                    alert("Tu anuncio ha sido actualizado con éxito");
                   window.location="areaC.php";
                 </script>
                    ';
                  die();               
                 

    }  


      if (isset($_POST['baja1'])) {

          borrarCuidador($cuidador['IdCui']);
           echo '
                  <script>
                    alert("El cuidador ha sido borrado con exito. Esperamos volver a verte pronto");
                    window.location="index.php";
                 </script>
                    ';
                                 
                 die();
                            
     }



?>


<!DOCTYPE html>
<html lang="es">
<head>

 
   <meta charset="utf-8">

   
    <script src="js/funciones.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/aceptaCont.js"></script>
    <script src="js/rechazaCont.js"></script>
    <script src="js/cancelaCont.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

    <script src=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js"></script>

   <link rel="stylesheet" href="css/estilos.css" />

 
  <style type="text/css">

 
 
 .area{
   margin-top: 20px; text-decoration: underline;
 }

 .titulos{
  text-decoration: wavy underline;
  text-align: center; 
  font-size: 30px;
  margin-bottom: 20px; 
  margin-top: 15px;

 }
  .nombre{
    margin-left: 20px;
    margin-bottom: 25px;
    margin-top: 20px;
    font-weight: bold;
    font-family: verdana;
     color:#009999;
  }

  .anun{
    margin-top: 30px;
    margin-bottom: 25px; 
    font-family: verdana; 
    color:#009999; 
    font-weight: bold;
  }

  #datos,#anuncio{
   width: 100%;
   padding:25px;
   background: #FFFFE0;
   border: 4px solid #009999;
   box-shadow: 15px 15px 15px 15px rgba(0,0,0,0.47);
   border-radius: 15px;

}
   textarea {
    display: block;
    margin-left: auto;
    margin-right: auto;
}

  #anuncio .titulos{
   text-align: center;

 }

 #anuncio p{

    font-family: verdana; 
    color:#009999; 
    font-weight: bold;
 }

  .comment{
    display: flex;
     justify-content: center;
     align-content: center;
  }

  .btn{
    margin-bottom: 5px;
    
 }

  .textinput {
      margin-left: auto;
      margin-right: auto;
       width: 100%;
     resize: both;
      border: 3px solid grey;
     }

  textarea {
    margin:auto; 
    display: flex;

  }

 .hscroll {
  overflow-x: auto; 
  overflow-y: auto;
  
}

 .hscroll::-webkit-scrollbar {
    width: 15px;     
    height: 15px;    
 }


 .hscroll::-webkit-scrollbar-thumb {
    background: #ccc;
    border-radius: 4px;
}

.hscroll::-webkit-scrollbar-thumb:hover {
    background: #b3b3b3;
    box-shadow: 0 0 2px 1px rgba(0, 0, 0, 1);
}

 .hscroll::-webkit-scrollbar-thumb:active {
    background-color: #999999;
}

 table{
  flex: box;
  border-radius: 10px 10px;
   }

 th{
    font-size: 20px;
   color:#FFFFE0;

    } 

  tr,td{
    text-align: center;
  }

  td{
     color: #3399FF;
     font-weight: bolder;
  }

section{
  
  margin-bottom: 25px;
  margin-left: 25px;
}


  </style>

</head>



 <body>
   
  <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
             <a class="nav-link" href="index.php" onclick="sesion();">Inicio</a>
        
           </li>


            <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ayuda</a>
                 <div class="dropdown-menu">
                    <a class="dropdown-item" href="ayudaCui.php">Ayuda Cuidador</a>
                    
                </div>
          </li>  

          

         <li class="nav-item ml-sm-4">
            <a href="cerrar_sesionC.php"  class="btn btn-primary" role="button">Cerrar sesión</a>
            
          </li>
          
           
           <li class="nav-item ml-sm-4"> 
            <form class="formubaja" method="POST">     
              <button type="submit" class="btn btn-danger" name="baja1" >Baja cuidador</button>
            </form>
          </li>

      
         </ul>
       </div>
       
     </nav>

   

      <section class="main row pt-2 mr-1" >


          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ml-2">

            <h2 class="area"><center>Mi área de cuidador</center></h2></br>



           <div class="container">
            
              <h4 class="nombre">Bienvenido: <?php echo($cuidador["NickCui"]) ?>  <i class="fa fa-address-book fa-lg" aria-hidden="true"></i></h4>
                               
              <br>
              <br>



           <form method="POST" id="formu" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

  
            <div id="datos">

       
              <h4 class="titulos">Mis datos  <i class="fa fa-id-card fa-sm" aria-hidden="true"></i></h4>


              <div class="form-row">
                <div class="form-group col-md-3">
                 <label for="inputCuidador">Cuidador</label>
                  <input type="text" class="form-control" name="nom" disabled="" value="<?php echo($cuidador["NickCui"]) ?>"  id="inputCuidador" >
               </div>
              
                

             </div>


            <div class="form-row">
               <div class="form-group col-md-4">
                <label for="inputNombre">Nombre</label>
                  <input type="text" class="form-control" name="nombre" disabled="" value="<?php echo($cuidador["NombreCui"]) ?>" id="inputNombre" >
               </div>
               <div class="form-group col-md-4">
                <label for="inputDir">Dirección</label>
                 <input type="text" class="form-control" name="dir" value="<?php echo($cuidador["DirCui"]) ?>" id="inputDir" >
               </div>

                <div class="form-group col-md-2">
                  <label for="inputLoc">Localidad</label>
                  <input type="text" class="form-control" name="loc" value="<?php echo($cuidador["LocCui"]) ?>" id="inputLoc" >
               </div>

            </div>

          <div class="form-row"> 
             <div class="form-group col-md-4">
              <label for="inputCorreo">Correo electrónico</label>
               <input type="text" class="form-control" id="inputCorreo" name="correo" value="<?php echo($cuidador["EmailCui"]) ?>" id="inputEmail" >
             </div>
            <div class="form-group col-md-4">
              <label for="inputTel">Teléfono</label>
               <input type="num" class="form-control" id="inputTel" name="tlf" value="<?php echo($cuidador["TlfCui"]) ?>" id="inputTel" > 
             </div>
            <div class="form-group col col-md-2">
              <label for="inputExp">Experiencia</label>
               <input type="num" class="form-control" id="inputExp" name="exp" value="<?php echo($cuidador["ExpCui"]) .' '. 'años' ?>" id="inputExp" >
             </div>
             <div class="form-group col-md-2 ">
              <label for="inputEdad">Fecha Nacimiento</label>
               <input type="num" class="form-control" id="inputEdad" name="fecha" disabled="" value="<?php echo($cuidador["NacimientoCui"]) ?>" id="inputEdad" > 
             </div>
           </div>

            <div class="form-row"> 
             <div class="form-group col-md-4">
              <label for="inputIban">IBAN</label>
               <input type="text" class="form-control" id="inputIban" name="iban" value="<?php echo($cuidador["IbanCui"]) ?>" id="inputIban" >
             </div>
            </div>

             </br>



          <div class="form-row"> 
             
             <div class="form-group col-md-4">
               <label for="inputCuidado"><strong>¿Qué tipo de cuidados prestas?</strong> </label></br>

                  <?php
                                                    
                        foreach ($cuidados as $indice => $registro) {  

                           if($cuidador['IdCuidado']==$registro['IdCuidado']){

                          ?>
                       
                        <label>
                       <?php echo '<input type="radio" name="cuidado" checked="checked" value="'.$registro['IdCuidado'].'">'. " " .$registro['NombCuidado'].''; ?>
                       </label></br>
                      <?php  
                       } else{  ?>
                        
                            <label>
                       <?php echo '<input type="radio" name="cuidado" value="'.$registro['IdCuidado'].'">'. " " .$registro['NombCuidado'].''; ?>
                       </label></br> 

                        <?php
                       }

                      }
                     ?>

             </div>

            
           <div class="form-group col-md-4">
            <label for="inputTipo"><strong>¿Qué tipo de mascota cuidas?</strong> </label></br>
                                      
                     <?php
                         
                         foreach ($mascotas as $indice => $registro) {

                            if($cuidador['IdMascota']==$registro['IdMascota']){

                          ?>
                         
                        <label>
                       <?php echo '<input type="radio" name="mascota" checked="checked" value="'.$registro['IdMascota'].'">'. " " .""  .  $registro['NombreMascota'].''; ?>
                       </label></br>
                      <?php  
                       }else{  ?>

                        <?php echo '<input type="radio" name="mascota" value="'.$registro['IdMascota'].'">'. " " .""  .  $registro['NombreMascota'].''; ?>
                       </label></br>
                     <?php
                       }

                      }
                     ?>
                  
              
            </div>

                       
               <div class="form-group col-md-4">
                 <label for="inputZona"><strong>Zona</strong></label></br>
                  <select name="zona">
                     <option value="">Seleccione zona:</option>

                      <?php  
                                                                    
                      foreach ($zonas as $indice => $registro) { 

                         if($registro['IdZona'] == $cuidador['IdZona']){
                       
                         echo "<option selected value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";

                         }else{
                         echo "<option value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";
                        }
                      
                      }
                     ?>

                   </select>

                </div>
            </div>

             <div class="form-row"> 
               <div class="form-group col-md-4">
                    <label for="inputPrecio"><strong>¿Qué precio por hora tienes?</strong> </label>
               <select name="precio">

                 <?php

                       $array =[3,4,6,7,8,9,10,12];



                       foreach ($array as $value) {
                          
                          if($value==$cuidador['PrecioH']){ ?>

                            <option value="<?php echo($value)?>" selected="selected"><?php echo($value)?> €/h</option>

                         <?php }else{ ?>

                             <option value="<?php echo($value)?>"><?php echo($value)?> €/h</option>

                       <?php  }

                          } 
                      ?>
                   
                    
                  </select>
             </div>
            </div>


               
              <form id='actual' method="POST">
                <div style="text-align: center;">
                  <input type="submit" value="Guardar cambios" name="actualizar" class="btn btn-primary">
                </div>
              </form>

               </div>
           </div>
        
        </form>

             </br>
             </br>
             </br>
          
           
           <div class="container">
                <h4 class="titulos">Mis anuncios</h4> 

                  <form id='anuncio' method="POST">
                        <p>Puedes modificar tu anuncio las veces que desees. Solo tienes que modificar tu anuncio y después "Guardar Cambios"</p>

                     <div class="form-row"> 
                        <div class="form-group col-md-12">
                          <div class="comment">
                             <textarea class="textinput" name="anuncio" id="areaAnuncio" rows="5" ><?php echo($cuidador["Anuncio"]) ?></textarea>
                          </div> 
                        </div>                   
                        
                   </div>

                  
                   <div style="text-align: center;">
                  
                   <input type="submit" value="Guardar cambios" class="btn btn-primary" name="guardarAnun" onclick="mostrar()">

                    
                   </div>

                </form>  
                                  

               </div>           
                    
                   
            </div>
          </div>
     

        </br>
        </br>
        </br>
           
             <div class="container">

              <div class="hscroll">
                         
               <h4 class="titulos">Mis contratos</h4>


              <div class="table-responsive-sx">   
               <table class="table table-hover table-dark">
                 <thead>
                    <tr>
                      <th>Num. contrato</th>
                      <th>Inicio contrato</th>
                      <th>Fin contrato</th>
                      <th>Aceptar</th>
                      <th>Rechazar</th>
                      <th>Detalles</th>
                      <th>Cancelar</th>
                   </tr>
                </thead>
                 <tbody>
                       <?php

                         if(empty($resulcontratos)){
                        ?>

                          <tr>
                            <td><?php echo '<div style="color:#00FF00;">¡Vaya! aún no tienes peticiones nuevas de contrato</div>'; ?>
                            </td>
                             <td><?php echo '<div style="color:#00FF00;">Pronto tendrás alguna peticion</div>'; ?>
                              
                             </td>

                          </tr>

                         <?php

                           }else{


                          foreach ($resulcontratos as $contrato) {
                                         

                         ?>

                    <tr id="lineacontrato<?php echo $contrato['IdContrato'] ?>">
                       <td><?php echo $contrato['IdContrato'] ?></td>
                       <td><?php echo $contrato['FechaHoraInicio'] ?></td>
                       <td><?php echo $contrato['FechaHoraFin'] ?></td>

                       <td class="aceptar"> 

                          <?php

                            if($contrato['Estado']=='aceptado'){

                               echo '<div style="color:#00FF00;">Aceptado</div>';
                            }
                            else{

                              if($contrato['Estado']==''){

                                ?>
                                 <button type="button" id="aceptar" class="btn btn-success" name="btnaceptar" onClick="aceptar_contrato(<?php echo $contrato['IdContrato'] ?>)"><i class="fas fa-check-square"></i></button>

                                  <?php

                              }
                            }
                            ?>
                           
                       </td>

                       <td class="rechazar">

                                  <?php

                            if($contrato['Estado']=='rechazado'){

                              echo '<div style="color:#FF3333;">Rechazado</div>';
                            }
                            else{
                                    
                              if($contrato['Estado']==''){

                                ?>
                                
                            <button type="button" id="rechazar" class="btn btn-danger" name="btnrechazar" onClick="rechazar_contrato(<?php echo $contrato['IdContrato'] ?>)"> <i class="fas fa-ban"></i></button>

                                  <?php

                              }
                            }
                            ?>

                       </td>
                          

                       <td> 
                         
                       <!-- Button to Open the Modal -->
                          <button type="button" class=" btn-primary btn-sm" name="ver" data-toggle="modal" data-target="#<?php echo "modalcomentarios" . $contrato['IdContrato'] ?>" >
                           <i class="fab fa-wpforms"></i>
                          </button>
                   

                       <!-- The Modal -->
                         <div class="modal fade" id="<?php echo "modalcomentarios" . $contrato['IdContrato'] ?>">
                           <div class="modal-dialog">
                           <div class="modal-content">
      
                             <!-- Modal Header -->
                             <div class="modal-header">
                                <h4 class="modal-title">Petición</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                             </div>
        
                               <!-- Modal body -->
                                                  
                              <div class="modal-body">
                                
                                <?php
                                  echo $contrato['Comentarios'];
                                ?>

                             </div>

                              <!-- Modal footer -->
                             <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                             </div>
        
                            </div>
                          </div>
                         </div>

                       </td>

                       
                        <td class="cancelar">

                                  <?php

                            if($contrato['Estado']=='cancelado'){

                              echo '<div style="color:#FFA500;">Cancelado</div>';
                            }
                            else{
                                    
                                
                                ?>
                                
                            <button type="button" id="cancelar" class="btn btn-warning" name="btncancelar" onClick="cancelar_contrato(<?php echo $contrato['IdContrato'] ?>)"> <i class="far fa-window-close"></i></button>

                            
                                <?php
                                 
                              }
                            ?>

                       </td>

                                                

                    </tr>

                     <?php
                           }
                         }
                        
                     ?>
                    
                </tbody>
             </table>
           </div>
          </div>
        </div>
              
          
            </br>

                     
       </article>

   </section>

 

         
  <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook"> <i class="fab fa-facebook-square " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram "></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square "></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>
  

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>