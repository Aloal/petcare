
<?php

  require("conexion.php");
  require("libreria/funciones.php");
  require ("libreria/regbdOpinion.php");
 
  session_start();

    // $usuario=getUsuario_por_nombre($_SESSION['usuario']);


     if (isset($_POST['opiniones'])) {
        
         registroOpinion();

    }



     if (isset($_GET['idavisoBorrar'])) {
        
          borrar_aviso($_GET['idavisoBorrar']);

          cancelar_contrato($_GET['idcontrato']);
    }



      $avisos=get_mensajes_contratos($_SESSION['idusu']);

  
       

    ?>


<!DOCTYPE html>
<html lang="es">

<head>


  <meta charset="utf-8">

     
      <script src="js/jquery.js"></script>
      <script src="js/funciones.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">



   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

     <link rel="stylesheet" href="css/estilos.css" />
   

<style type="text/css">


 th{
    font-weight: bolder;
    font-size: 18px;
 }


tr{
  align-content: center;
}

table{
   border-style: black 6px solid;
   box-shadow: 10px 10px 14px 2px rgba(0,0,0,0.47);
}


article h4{
 margin-bottom: 40px;
 margin-top: 25px;
 color:#009999;

}

 
.primera{
  display: flex;
  justify-content: center;
  align-items: center;
}

.titulo2{
      color:#009999;
  margin-bottom: 40px;
  font-weight: bold;
}

 
 #opinion{
   border-radius: 10px;
   padding: 7px;
 }

 #area{
   display: none;
 }

 .borrar{
  width: 30px;
  height: 30px;
  color: red;
 }

 .btn{
    margin-bottom: 5px;
    
 }

 
 table{
  flex: box;
  border-radius: 10px 10px;
   }
  

 
 </style>

 </head>


<body>

   
 <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
             <a class="nav-link" href="index.php"onclick="sesion();">Inicio</a>
           </li>

          

           <li class="nav-item ml-sm-4">
            <a href="cerrar_sesionU.php"  class="btn btn-primary" role="button">Cerrar sesión</a>
            
          </li>

      
         </ul>
       </div>
       
     </nav>

   
      


       <script type="text/javascript">
         
            
          $(document).ready(function(){
                $("#opinar").click(function() {
                 $("#area").show("slow");
               });

                $("#envio").click(function() {
                 $("#area").hide(1500);
              });

            });

                    

           </script>


               
  
 
      <section class="main row pt-2 mr-1" >


         <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ml-2">

           <div class="container">


            <h4><strong>Mis avisos</strong></h4>

            <div class="titulo2">Aqui podrás ver en que estado se encuentra tu petición, si ha sido aceptada, rechazada o cancelada. También puedes dejar tus opiniones sobre el servicio prestado por el cuidador, una vez el servicio haya sido prestado.</div>

            <div class="container">

              <div class="hscroll">
                     
                                                     
               <div class="table-responsive-sx">   
                <table class="table table-hover table-dark">
                  <thead>
                       <tr>
                          <th style="text-align: center;">Notificaciones</th>
                          <th>Cancelar</th>
                          <th>Opinar</th>
        
                       </tr>
                </thead>
                <tbody>

                         <?php

                           if(empty($avisos)){
                        ?>

                          <tr>
                            <td><?php echo '<div style="color:#00FF00;">¡Vaya! aún no tienes ningún aviso</div>'; ?>
                            </td>
                             <td><?php echo '<div style="color:#00FF00;">Quizás no hayas hecho peticiones de contrato o puede que tus peticiones estén pendientes de aceptar aún</div>'; ?>
                              
                             </td>

                          </tr>

                         <?php

                           }else{

                          
                            foreach ($avisos as $aviso) {

                              $datoscontra=get_datos_contrato($aviso['IdContrato']);
                             
                        ?>

                 <tr>
                   
                      <td ><i class="fas fa-hand-point-right">  </i><?php echo $aviso['ComentAviso']?></td>

                       
                       <td style="text-align: center;">    

                         <form name="formucancel"  method="POST">

                           <a href="avisos.php?idavisoBorrar=<?php echo $aviso['IdAvisos'] ; ?>&idcontrato=<?php echo $aviso['IdContrato']; ?>" class="btn-danger btn-sm"><i class="far fa-window-close"></i></a>
                               
                         </form>
                         
                       </td>




                       <td style="text-align: center;">

                                                                                                                  
                       <!-- Button to Open the Modal -->
                          <button type="button" class=" btn-primary btn-sm" name="ver" id="btnopinar" data-dismiss="modal" data-toggle="modal" data-target="#<?php echo "modalcomentarios" . $aviso['IdAvisos'] ?>" >
                           <i class="far fa-comment-dots"></i>
                          </button>

                               

                       <!-- The Modal -->
                         <div class="modal fade" id="<?php echo "modalcomentarios" . $aviso['IdAvisos'] ?>">
                           <div class="modal-dialog">
                           <div class="modal-content">
      
                             <!-- Modal Header -->
                             <div class="modal-header">

                              <?php

                                   $cuidadoropinion=getCuidador_por_id($datoscontra['IdCui']); 

                                ?>


                              <h4 class="modal-title">Opinión sobre: <?php echo $cuidadoropinion['NickCui'] ?> </h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                             </div>
        

                               <!-- Modal body -->
                                                  
                              <div class="modal-body">
                                 <form name="formuopinion"  method="POST">
                                                    
                                 <div> <textarea  id="opinion" name="opinion" cols="45" rows="5" ></textarea> </div>
                         
                                   <input type="hidden"  name="cuidador" style="margin-right: 35px; " value="<?php echo($datoscontra["IdCui"]) ?>">


                                  <input type="submit" id="opiniones" name="opiniones" style="margin-top: 25px; margin-bottom: 25px;"  value="Enviar opinión" class="btn btn-success">
        
                                </form>



                             </div>

                              <!-- Modal footer -->
                             <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                             </div>
        
                            </div>
                          </div>
                         </div>


                       </td>

                  </tr>

                      <?php
                         
                       }
                     }
                    ?>

                 
                 </tbody>
               </table>

              </div>
            </div>
           </div>

                   </br>

                                     
          </div>   
 

         </article>

      </section>
               
      
  

            <div class="btn" style="color: #B266FF; margin-bottom: 8px; margin-left: 10px;">
              <a href="areaU.php"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver</strong></a></div>

   <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram "></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square" ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>
  

   <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
   <script src="js/bootstrap-datetimepicker.min.js"></script>

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  </body>

</html>