<?php
require("conexion.php");
require("libreria/funciones.php");

aceptarCont($_GET['IdContrato']);                     // pones como aceptado el contrato

   $datosContrato=get_datos_contrato($_GET['IdContrato']);   //Obtienes todos las datos del contrato

    
crear_mensaje('El contrato' .' '. $datosContrato['IdContrato'] . ' '. 'con fechas de inicio y fin' .' '. $datosContrato['FechaHoraInicio'] . ' ' .'------ '. $datosContrato['FechaHoraFin'] . ' ' . 'ha sido aceptado por el cuidador, el día antes del servicio contactará contigo. Recuerda que si por cualquier motivo tuvieses que cancelar, dispones de hasta 48 horas antes del servicio ¡Muchas gracias!' , $datosContrato['IdUsu'], $_GET['IdContrato'] );                                    


//Creas el aviso en base de datos


header('Content-Type: application/json');
echo json_encode(['ok'=> true, 'id'=>$_GET['IdContrato']]);

?>