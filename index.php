
<?php
require("conexion.php");
require ("libreria/funciones.php");

   $zonas=getZona();

    if(isset($_SESSION['usuario'])){
     header("location: areaU.php");
      die();
     }

   
?>


<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
   
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
   
   <script src=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js"></script>

  <link rel="stylesheet" href="css/estilos.css" />
 
<style>

      
  img{
  max-width: 100%;
 }
  

.bt3{background-color:#009999;
  color:white;width:220px;
     font-size: 20px;
     
 }

  .form-inline{ margin-bottom: 20px;

 }

 .btn{
    margin-bottom: 5px;
    
 }


#principal{
    position: relative;
    display: inline-block;
    text-align: center;
    margin-top: 14px;
    margin-left: 10pX;
    margin-bottom: 14px;
    
   border-radius: 30px 30px;
}

 
 
 .centrado{
    position: absolute;
    top: 70%;
    left: 70%;
    transform: translate(-50%, -50%);
}

 
 aside li{
   font-size: 16px;
   text-decoration: none;
  
 }


         
</style>


</head>



<body>

 <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
             <a class="nav-link" href="index.php">Inicio</a>
           </li>

           <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ayuda</a>
                 <div class="dropdown-menu">
                     <a class="dropdown-item" href="ayudaUsu.php">Ayuda Usuarios</a>
                     <a class="dropdown-item" href="ayudaCui.php">Ayuda Cuidadores</a>
                </div>
          </li>             

          <li class="nav-item ml-sm-4">
           <a href="formU.php " class="btn btn-primary">Regístrate</a>
                    
          </li>

          <li class="nav-item ml-sm-4">
            <a href="logU.php"  class="btn btn-success">Mi Área</a>
                    
          </li>

      
         </ul>
       </div>
       
     </nav>


      
   <section class="">

   <div class="main row pt-2 mr-3 " >

      <article class=" .d-none .d-sm-block col-sm-11 col-md-11 col-lg-9 ml-2" style="width: auto;">
   

            <div id="principal" style="width: auto;">
            <img  class=".d-none .d-sm-block" src="imagenes_video/fondo4.png" alt="mascotas" style="width:1000px; height: 435px; border-radius: 30px 30px; box-shadow: 6px 6px 6px 6px rgba(0,0,0,0.2);">  
              
  
            <div class="centrado" style="font-size: 22px;"><strong>¡Empieza a buscar ya tu cuidador en Asturias!</strong></div>
                     
            </div> 

       </article>

        <aside class="col-md-2 col-lg-2  d-none d-lg-block">
                    
              
              <div class="list-group pt-4 mr-4">
                  
                <ul class="columna">
                  <li class="list-group-item list-group-item-secondary"><a href="html/cuidadosmascota.html"><strong>Cuidados para tu mascota</strong></a></li>

                 
                  <li class="list-group-item list-group-item-secondary"><a href="html/articulos.html"><strong>Articulos sobre animales de compañía</strong></a></li>

               </ul>  
              </div>

              <div class=".d-none .d-lg-block .d-xl-none"> <h5 style="color:#009999; ">¡El mejor cuidado de mascotas para tu amigo!</h5><strong>En PET CARE podrás encontrar tu cuidador ideal en pocos clicks y filtrando por tipo de cuidado, tipo de mascota, zona y precio. </strong></div>


        </aside>

   </div>

  <section>
    <div class="row align-items-start" style="margin-right: 15px;">
      <div class="col-xs-12 col-sm-12 col-md-4  pt-4 mt-1 ml-3 ">

         <h3 style="color:black;"><u>Necesito un cuidador en Asturias</u></h3>
   

        <p>Accede a nuestro buscador para empezar a buscar el cuidador para tu mascota en la zona  que desees.</p>

        
       
        <p class="btbuscar" style="font-size:20px;"><strong>¿En qué zona necesitas cuidador?</strong></p>

         <form class="form-inline" action="buscar.php" method="GET">
                     
            <div class="form-group col-md-4">
                
                  <select name="zona">
                     <option value="">Seleccione zona:</option>

                      <?php  
                                                                    
                      foreach ($zonas as $indice => $registro) { 
                       
                         echo "<option value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";
                      
                      }
                     ?>

                   </select>

                </div>
               
            <p style="font-size: 17px; margin-top: 10px;"><strong>100% Gratuito sin compromiso <input type="submit" name="boton3"  value="Buscar Cuidador" class="btn btn-primary btn-lg" style=" margin-top: 15px; margin-left: 20px;"></strong></p>
             

          </form>

             
        

         <p><strong>La experiencia mas amplia.</strong> Más de 10 años cuidando mascotas.</p>

         <p>Si aún no tienes cuenta de usuario no esperes más,<a href="formU.php" style="text-decoration: none;"><strong> Regístrate</strong></a>  y empieza a contactar con cuidadores para tu mascota con los distintos <strong>filtros.</strong> </p>

           
            <p><strong>¡Busca tu cuidador ya! Flexibilidad y confianza</strong></p><br/>
        
        

      </div>
     


        <div class="col-xs-12 col-sm-12 col-md-4  mt-4 mb-1 ml-3" >
          
          <h3 style="color:black; margin-bottom: 12px;"><u>4 Tipos de cuidados</u></h3>

            <p><strong>Cuidado con familia anfitriona.</strong>Deja tu mascota con una familia anfitriona y asegurate de que pase tus vacaciones en buenas manos.   <i class="fas fa-dog fa-xl"></i>  <i class="fas fa-cat fa-xl"></i></p><br/>

            <p><strong>Cuidado de mascotas a domicilio.</strong>Nuestro cuidador se quedará en tu propio domicilio mientras tu estas de viaje. <i class="fas fa-dog fa-xl"></i>  <i class="fas fa-cat fa-xl"></i></p><br/>

            <p><strong>Acompañamiento veterinario.</strong>El cuidador acompañará a tu mascota al veterinario si tu no tiene tiempo para llevarla.  <i class="fas fa-dog fa-xl"></i>  <i class="fas fa-cat fa-xl"></i></p><br/>

            <p><strong>Paseo de perros.</strong>¿Trabajas todo el día y vives que no tienes tiempo para atender tu mascota?, Nuestros paseadores de perros se encargarán de sacar a tu mascota y darle una vuelta por la zona.  <i class="fas fa-dog fa-xl"></i></p><br/>

            

        </div>


      <div class="col-xs-12 col-sm-12 col-md-3 pt-3 mt-2 ml-3">

        <h3 style="color:black"><u>Hazte cuidador y regala cariño</u></h3>

        <p>¿Te gustaría ser cuidador?,¿Te gustan los animales y querrías cuidar perros y gatos de vez en cuando. Conviértete en miembro cuidador de PET CARE y consigue ingresos extra regularmente.</p>

            <p><a href="formC.php" class="btn btn-primary btn-lg">Hazte Cuidador</a></p>
             
            
            <p style=" padding: 2px;" >Una vez tengas tu perfil de cuidador colaborador en PET Care, te podrán contactar dueños de mascotas.</p> 


            <p>Si ya eres cuidador colaborador, puedes entrar en tu área personal.</p>

            <p><a href="logC.php" class="btn btn-success btn-lg">Área cuidador</a></p>
             


              
      </div>

    </div>
</section>

 
 <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#252e39"
    },
    "button": {
      "background": "#14a7d0"
    }
  },
  "showLink": false,
  "theme": "classic",
  "position": "bottom-right",
  "content": {
    "message": "Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web. ",
    "dismiss": "Aceptar"
  }
});
</script>
  


        
    <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
          <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram"> <i class="fab fa-instagram " ></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square" ></i></a>

            </p>
          </div>
         </div>
 
 
     </div>
    </div>
  </footer>
  
  
  
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  
 </body>

</html>