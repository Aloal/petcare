<?php

require("conexion.php");
require ("libreria/funciones.php");


  session_start();

     
     $todasOpiniones=getOpiniones_contrato($_GET['Idcui']);


    if(empty($todasOpiniones)){
        echo '
         <script>
             alert("¡Lo sentimos!, actualmente no hay opiniones sobre este cuidador. ");
             history.go(-1);
        </script>';
   
      
   }
      

?>


<!DOCTYPE html>
<html lang="es">
<head>

   <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

 
<link rel="stylesheet" href="css/estilos.css" />

  <style type="text/css">


   aside{
       margin-top: 150px;
     margin-left: 40px;
   }


aside p{
   color: #009999;
   margin-left: 12px;
   margin-top: 70px;
   font-size: 16px;
   float:right;
 }

  aside .sidebar{
 position: sticky;
 top:45px;
 
 }

 article h4{
   margin-top: 25px;
   }


#cuidador{
  position: center;
  margin-top: 40px;
  margin-bottom: 15px;
}

section{
  
  margin-bottom: 25px;
  margin-left: 25px;
}

.btn{
    margin-bottom: 5px;
    
 }

  </style>

</head>

<body>
   
     <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
             <a class="nav-link">Inicio</a>
           </li>

           <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ayuda</a>
                 <div class="dropdown-menu">
                     <a class="dropdown-item" href="ayudaUsu.php">Ayuda Usuarios</a>
                     <a class="dropdown-item" href="ayudaCui.php">Ayuda Cuidadores</a>
                </div>
          </li>   
                 
               <li class="nav-item ml-sm-4">
                 <a href="cerrar_sesionU.php"  class="btn btn-primary" role="button">Cerrar sesión</a>
              </li>
                
         </ul>
       </div>
       
     </nav>

   


    <section class="main row pt-2 mr-1" >


          <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ml-2">

         
           <div class="container mt-3">
               <h4 style=" font-family: verdana; color:#009999; margin-bottom: 35px; margin-top: 35px; margin-left: 30px;"><strong>Estas son las opiniones sobre el cuidador  <?php echo($_GET["nick"]) ?> </strong></h4>
   
                <br>

                   
              <?php 
                                   
                foreach ($todasOpiniones as $opiniones) {
                    
                    ?>

                 <div class="media border p-3" style="  box-shadow: 6px 6px 6px 6px rgba(0,0,0,0.2); border-radius: 10px 10px 10px 10px; background:#FFCC99; margin-bottom: 20px; width: 95%; ">

                       
                      <img src="imagenes_video/avatarchico.png" alt="chico" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                        

                       <div class="media-body">
                        <h4> <small><i style="color: #0000FF"><?php echo $opiniones['NickUsu'] ?>--->   Publicado en fecha: <?php echo $opiniones['Fecha'] ?> </i></small></h4>
                           <p>" <?php echo $opiniones['OpComent'] ?> "</p>
                       </div>  
                 </div>


               <?php
                   }
                ?>


            </div>
         

    
         </article>


         <aside class=" col-md-3 col-lg-3  d-none d-md-block "> 

            <div class="sidebar .d-sm-none .d-md-block hidden-xs">


                <img style="margin-left: 15px; margin-top: 40px;  max-width:100%;
               height: auto;" id="cuidador2" src="imagenes_video/lapiz.png" alt="lapiz" class="img-fluid rounded-circle visible only on d-none d-sm-inline" width="200" height="200"> 

                  <p><strong>Son muchos los usuarios que dejan opiniones sobre los cuidadores, no dudes en consultar las opiniones de sobre ellos, te ayudarán a contar con el que consideres el mejor cuidador para tu mascota.</strong> </p>

              
               <img style="margin-left: 15px; margin-top: 40px;  max-width:100%;
               height: auto;" id="cuidador2" src="imagenes_video/opiniones.png" alt="hojaopinion" class="img-fluid rounded-circle visible only on d-none d-sm-inline" width="200" height="200"> 

           </div> 

              
        </aside>

       
   </section>

 


          <div class="btn" style="color: #B266FF;">
            <a href="areaU.php"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver</strong></a> 
          </div>    
   <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square fa-xl" ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram fa-xl" ></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square fa-xl" ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>
  
 


   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>

	



