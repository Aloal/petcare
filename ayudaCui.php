
<!DOCTYPE html>
<html lang="es">
<head>

  <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

  <link rel="stylesheet" href="css/estilos.css">


  <style type="text/css">
    
 

article p{
   margin-right: 100px;
   margin-left: 100px;
}

 article h5{
 
 color:#009999;
 margin-left: 90px; 
 margin-bottom: 15px;
 margin-top: 15px;
 }

  
.form {
  width: 100%;
  max-width: 600px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  margin-top: 15px;
  margin-bottom: 15px;
}

#cuidador{
  position: center;
  margin-top: 40px;
  margin-bottom: 15px;
}

 .btn{
    margin-bottom: 5px;
    
 }

section{
   margin-top: 25px;
  margin-bottom: 25px;
  margin-left: 25px;
}


  </style>

</head>


<body>
   
   
  <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

           <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
             <a class="nav-link" href="index.php">Inicio</a>
           </li>

           <li class="nav-item ml-sm-4">
           <a href="formC.php " class="btn btn-primary">Regístrate</a>
                    
          </li>

          <li class="nav-item ml-sm-4">
            <a href="logC.php"  class="btn btn-success">Mi Área</a>
                    
          </li>

      
         </ul>
       </div>
       
     </nav> 

               
 
      <section class="main row pt-2 mr-1" >


          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ml-2">

           <h5><strong>¿Cómo puedo formar parte de la comunidad de cuidadores PET CARE?</strong></h5>
           <p class="uno">Muy fácil: tan solo tendrás que darte de alta en la seccion <a href="formC.php"><strong>HAZTE CUIDADOR</strong></a>  y los usuarios ya podrán empezar a contactarte. Rellenarás un formulario con algunos datos importantes para los usuarios a la hora de encontrar su cuidador ideal y también tendrás un anuncio que los usuarios podrán ver con tu perfil antes de decidir contactarte. ¡Además darte de alta es totalmente gratuito!</p>

           <h5><strong>¿Como es el sistema de cobro, cuándo cobraré, puedo establecer el precio que yo quiera?</strong></h5>
           <p class="dos"> Una vez hayas "contratado" con el usuario y prestado el servicio, PET Care te ingresará en la cuenta bancaria que nos hayas facilitado a tal efecto el precio del servicio que hayas prestado dentro de los 15 dias siguientes a la finalización del mismo, de acuerdo a la tarifa que tengas establecida en tu perfil. A este precio total solamente se te detraerá un 2% en concepto de comisión de gestión. </p>
           <p>En Pet CARE creemos que todo el mundo tiene derecho a tener un cuidador de mascotas independientemente de su capacidad económica, por eso hemos diseñado un rango de precios muy amplio; tú como cuidador al darte de alta deberás de escoger el precio que tu consideras adecuado según tu experiencia, cualidades y tus servicios, dentro del abanico de precios que se ofrecen.</p>
           <p>Los precios finales a cobrar tendrán uno descuentos en funcion del número de horas contratadas, la escala será la siguiente:</p>
           <p> a) Si el contrato es por menos de 24 horas, el precio será el que tu hayas elegido en tu perfil por la horas de servicio.</p>
           <p> b) Si el contrato tiene un duración de entre 24 y 96, el precio final serán las horas contratadas por el precio hora que tu, cuidador tengas establecido en tu perfil, pero al 60% del total.</p>
           <p> c) Si el contrato tiene un duración de más de 96 horas, el precio final serán las horas contratadas por el precio hora que tu, cuidador tengas establecido en tu perfil, pero al 40% del total.</p>

                      
           <h5><strong>¿Pueden contactarme varios usuarios?</strong></h5>
           <p class="tres">Por supuesto, pueden contactarte los usuarios que deseen, las veces que quieran. En tu area de cuiadador te aparecerá un contacto, tu podrás ver el contacto y deberás aceptar la solicitud, posteriormente Pet CARE os facilitará una forma de contacto para el servicio.
           Por motivos de eficiencia no se podrá prestar mas de un servicio el mismo día, por tanto a la hora de aceptar solicitudes deberás tener en cuenta que no tengas ya otro servicio ese día contratado.
           Si tu no desseas prestar el servicio por el motivo que sea, simplemente no aceptarás la solicitud y el usuario sabrá que no ha sido aceptada.</p>

        
             
            <h5><strong>¿Podría contactarme un usuario fuera de este canal?</strong></h5>
           <p class="cuatro">Al entrar a formar parte de la comunidad Pet CARE firmarás un contrato con la empresa donde se establece la exclusividad del servicio. </p>


             <h5><strong>¿Cómo puedo ponerme en contacto con Pet Care?</strong></h5>
           <p class="cinco"> Para cualquier problema que surja como pérdida de contraseñas, peticiones de reestablecimiento de las mismas, quejas, etc....,  puedes ponerte en contacto con nosotros por medio de nuestro corre electrónico <strong> <a href="mailto:petcare@gmail.com" style="text-decoration: none; color:black">petcare@gmail.com  <i class="far fa-envelope"></i> </a></strong>, identificándote y explicádonos cualquier situación.</p>
           
           
          
           </br>

           <div class="btn" style="color:black; margin-bottom: 8px;">
             
               <a href="javascript: history.go(-1)"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver</strong></a> 

           </div>

       </article>


   </section>

 

      <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook"> <i class="fab fa-facebook-square fa-xl" ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram fa-xl" ></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square fa-xl" ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>


   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>