
<?php

require("conexion.php");
require ("libreria/funciones.php");

  


  $resultados=getCuidado_zona_mascota_precio($_GET['cuidado'],$_GET['zona'],$_GET['mascota'],$_GET['precio']);

   
    if(empty($resultados)){
           header("location: noHay.php");
      die();
         
     }

       
?>


<!DOCTYPE html>
<html lang="es">
<head>

   <meta charset="utf-8">
    
     <script src="js/jquery.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

    <script src=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js"></script>

   <link rel="stylesheet" href="css/estilos.css" />
  

 
  <style type="text/css">
    

section{
  align-content: center
  margin-bottom: 25px;
 
}

article,h3{
	color:#009999;
	align-content: center; 
	margin-bottom: 45px;
	margin-top: 20px;
   align-content: center
}




article u{
text-decoration: underline; 
color: #000000;

}

aside{
   margin-top: 100pX;
 }


 aside .sidebar{
 position: sticky;
 top:50px;

 }

.card{
   margin-bottom: 30px;
   margin-right: 20px;
   max-width: 100%; 
   height: auto;
   background-color: #FFFFE0;
   border: black solid 3px;
}
 

    #subir_arriba{
    width: 30px;
    height: 30px;
    background: yellow;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 20px;
    cursor:pointer;
    position: fixed;
      

   }


  .btn{
    margin-bottom: 5px;
    
 }
  </style>

</head>

<body>
   
   
  <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
             <a class="nav-link" href="index.php">Inicio</a>
           </li>

           <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ayuda</a>
                 <div class="dropdown-menu">
                     <a class="dropdown-item" href="ayudaUsu.php">Ayuda Usuarios</a>
                    <a class="dropdown-item" href="ayudaCui.php">Ayuda Cuidadores</a>
                </div>
          </li>             

           <li class="nav-item ml-sm-4">
            <a href="cerrar_sesionU.php"  class="btn btn-primary" role="button">Cerrar sesión</a>
            
          </li>

               
         </ul>
       </div>
       
     </nav>


  
  	 <div class="container-fluid row pt-2 mr-1" >

  	 	
          <article class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xl-8 ml-4 flex">

           <h3 class="d-none d-sm-block "><strong>Estos son los cuidadores disponibles en tu zona</strong></h3>
         
              <?php 

               $date_now = date('d-m-Y');
               $date_past = strtotime('-15 day', strtotime($date_now));
               $date_past = date('d-m-Y', $date_past);


                                   
                foreach ($resultados as $resultado) {
                    
                    ?>

                    <div class="card" style="width:371px; display: inline-block; ">
                     
                      <?php 
                         if ($resultado['Sexo'] == 'Hombre') {
                          ?>

                           <img class="card-img-top" src="imagenes_video/avatarchico.png" alt="chico" style="width: 125px; height: 125px;"><?php cuenta_contratos($resultado['IdCui'],$date_past)?>
                           <?php 
                         }
                            ?>

                       <?php 
                          if ($resultado['Sexo'] == 'Mujer') {
                          ?>

                           <img class="card-img-top" src="imagenes_video/avatarchica.png" alt="chica" style="width: 121px; height: 125px;"><?php cuenta_contratos($resultado['IdCui'],$date_past)?>
                          <?php 
                           }
                            ?>

                 
                    <div class="card-body ">

                     <u>Alias:</u> <?php echo $resultado['NickCui'] ?> </br> 
                     <u>Sexo:</u> <?php echo $resultado['Sexo'] ?> </br>
                     <u>Localidad:</u> <?php echo  $resultado['LocCui'] ?> </br>
                     <u>Años de experiencia:</u>  <?php echo $resultado['ExpCui'] ?> años </br> 
                     <u>Fecha Nacimiento:</u>  <?php echo $resultado['NacimientoCui'] ?>  </br>
                     <u>Precio servicos: </u>  <?php echo $resultado['PrecioH'] ?>  €/hora </br>
                     <u>Cuidados:</u>  <?php echo $resultado['NombCuidado'] ?>  </br>
                     <u>Mascotas que cuido: </u>  <?php echo $resultado['NombreMascota'] ?>  </br>
                     <u>Zona donde presto servicios:</u>  <?php echo $resultado['NombreZona'] ?> </br>
                    
                   </div>
                    

                      
                       <!-- Button to Open the Modal -->
                    <button type="button" class=" btn-success btn-sm"  name="btinformacion" data-toggle="modal" data-target="#myModal<?php echo($resultado['IdCui'])?>" name="anunciar" style="margin-left: 6px;">
                     <i class="fa fa-address-card" aria-hidden="true"></i>  Ver anuncio
                     </button>
                   

                       <!-- The Modal -->
                        <div class="modal fade" id="myModal<?php echo($resultado['IdCui'])?>">
                          <div class="modal-dialog">
                           <div class="modal-content">
      
                             <!-- Modal Header -->
                             <div class="modal-header">
                                <h4 class="modal-title"><?php echo $resultado['NickCui'] ?> </h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                             </div>
        
                               <!-- Modal body -->
                                                  
                              <div class="modal-body">
                                
                                <?php
                                  echo $resultado['Anuncio'];
                                ?>

                             </div>

                              <!-- Modal footer -->
                             <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                             </div>
        
                          </div>
                        </div>
                      </div>

                    
                    
                  <a href="opiniones.php?Idcui=<?php echo $resultado['IdCui'] ; ?>& nick=<?php echo $resultado['NickCui']; ?>" class="btn-primary btn-sm" style=" text-decoration: none; margin-top: -5px; margin-left:5px" >Opiniones <i class="fas fa-comment-dots"></i></a>
                                  


                  <a href="formContacto.php?Idcui=<?php echo $resultado['IdCui']; ?>& nick=<?php echo $resultado['NickCui']; ?>&precio=<?php echo $resultado['PrecioH']; ?>" class="btn-info btn-sm" style=" text-decoration: none; margin-top: -5px; margin-left: 5px;" >Contactar <i class="far fa-envelope"></i></a>
                     
                 </div>
                                          
               <?php
                   }
                ?>

    
                             
      </article>

       	   <aside class=" col-xl-3 ml-7 visible only on d-none d-xl-block "> 

            <div class="sidebar">


                <img  style="margin-left: 15px; max-width:100%; 
                height: auto;" id="cuidador" src="imagenes_video/cuidador.png" alt="cuidador" class=" img-fluid rounded-circle visible only on d-none d-xl-block" width="200" height="200">  



               <p style="margin-left: 25px; margin-top: 60px; max-width:100%; "><strong>Aquí están los cuidadores de tu zona. No esperes más para contactar con uno de ellos y contratar sus servicios. Recuerda que puedes enviar las peticiones de contrato que desees, nuestros cuidadores te responderán lo antes posible.</strong> </p>

              

               <img style="margin-left: 15px; margin-top: 40px;  max-width:100%;
               height: auto;" id="cuidador2" src="imagenes_video/cuidador2.png" alt="logo" class="img-fluid rounded-circle visible only on d-none d-xl-block" width="200" height="200"> 

           </div> 

              
        </aside>

                 <div id="subir_arriba">
                     <i class="fas fa-angle-up"></i>
                  </div>
     
     </div>



         <script type="text/javascript">
         
        let caja=document.getElementById("subir_arriba");
        caja.addEventListener("click", function(){
          window.scrollTo(0,0);
        })

        window.addEventListener("scroll",function(){


           if (window.scrollY>175) {

               caja.style.display="flex"

           }else{
                 caja.style.display="none"
           }
        })

       </script>


       
  

     <div class="btn" style="color: #B266FF;">
            <a href="areaU.php"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver</strong></a> 
          </div>    

     

         
     <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram " ></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square" ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>
 

  

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>