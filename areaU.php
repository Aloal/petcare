<?php


  require("conexion.php");
  require("libreria/funciones.php");
  require ("libreria/borrarUsuario.php");

  session_start();


  if(!isset($_SESSION['usuario'])){
     header("location: logU.php");
      die();
     }

      $cuidados=getCuidado();
      $mascotas=getMascotas();
      $zonas=getZona();


    
     $usuario=getUsuario_por_nombre($_SESSION['usuario']);


     if (isset($_POST['actualizar'])) {
        
        update_usuario($usuario['IdUsu']);

       $usuario=getUsuario_por_nombre($_SESSION['usuario']);

      }


     if (isset($_POST['baja'])) {

          borrarUsuario($usuario['IdUsu']);
           echo '
                  <script>
                    alert("El usuario ha sido borrado con exito. Esperamos volver a verte pronto");
                    window.location="index.php";
                 </script>
                    ';
                                 
                 die();
                            
     }

?>


<!DOCTYPE html>
<html lang="es">
<head>

   <meta charset="utf-8">
   
    <script src="js/funciones.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

   <script src=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js"></script>
  
   <link rel="stylesheet" href="css/estilos.css" />
 
  <style type="text/css">
    



  .contenedor1 h2{
   text-decoration: underline;
   margin-bottom: 35px; 

  }

 .contenedor2{
  text-align: center;
  margin-left: 35px;
  margin-right: 35px;

 }
 
 .contenedor2 h3{
  font-family: verdana;
   color:#009999;
 }

.form {
  width: 100%;
  max-width: 600px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  margin-top: 15px;
  margin-bottom: 15px;
  margin left: 15px;
  margin-right: 15px;
  border-style: dotted;
}

 .formu{
    box-shadow: 0px 0px 8px 8px rgba(0,0,0,0.47);  
     border-radius: 15px 15px 15px 15px;
   background: #FFFFE0;
   border: 4px solid #009999;
   font-size: 17px;

 }

#cuidador{
  position: center;
  margin-top: 40px;
  margin-bottom: 15px;
}

  .miarea{
 margin-left: 20px; 
 margin-bottom: 30px;
 margin-top: 25px; 
 font-weight: bold; 
 font-family: verdana; 
 color:#009999;

 }

 .vip{
       margin-top: 20px;
  margin-bottom: 20px;
  align-content: center;
  font-family: verdana;
   
 }

 .btn{
    margin-bottom: 5px;
    
 }

 .second-button{
    float:right;
}

 label{
  font-family: verdana;
   color:#009999;
 }

 
section{
  
  margin-bottom: 25px;
  margin-left: 25px;
}


  </style>

</head>


  <body>
   
  <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
              <a class="nav-link" href="index.php" onclick="sesion();">Inicio</a>
           </li>

            <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ayuda</a>
                 <div class="dropdown-menu">
                    <a class="dropdown-item" href="ayudaUsu.php">Ayuda Usuarios</a>
                    
                </div>
          </li>  

                      
          <li class="nav-item ml-sm-4">
            <a href="cerrar_sesionU.php"  class="btn btn-primary" role="button">Cerrar sesión</a>
            
          </li>
          
           
           <li class="nav-item ml-sm-4"> 
            <form class="formubaja" method="POST">     
              <button type="submit" class="btn btn-danger" name="baja" >Baja Usuario</button>
            </form>
          </li>
               
      </ul>
           
                          
                 
       </div>
       
     </nav>
 
     <script type="text/javascript">
       

    

     </script>


      <section class="main row pt-2 mr-2" >


          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ml-2">

          <div class="contenedor1">

           <div class="container-fluid">
            <h2><center>Mi área personal</center></h2></br>

           <h3 class="miarea">Bienvenido: <?php echo($usuario["NickUsu"]) ?>  <i class="fa fa-id-card fa-lg" aria-hidden="true"></i> <a href="avisos.php" role="button" class="btn btn-success" style="align-content: center; margin-left: 60px;">Notificaciones <i class="fas fa-comment-dots"></i></a></h3>   



            <form method="POST" id="formu" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

                
               <div class="form-row">
                <div class="form-group col-md-4">
                 <label for="inputUsuario">Usuario</label>
                  <input type="text" class="form-control" name="nom" value="<?php echo($usuario["NickUsu"]) ?>"  id="inputUsuario" disabled>
               </div>
               
                
               </div>

             <div class="form-row">
               <div class="form-group col-md-3">
                <label for="inputNombre">Nombre</label>
                  <input type="text" class="form-control" name="nombre" value="<?php echo($usuario["NombreUsu"]) ?>" id="inputNombre" disabled>
               </div>
               <div class="form-group col-md-4">
                <label for="inputDir">Dirección</label>
                 <input type="text" class="form-control"  name="dir" value="<?php echo($usuario["DirUsu"]) ?>" id="inputDir" >
               </div>
                <div class="form-group col-md-3">
                <label for="inputDni">DNI</label>
                 <input type="text" class="form-control" name="dni" value="<?php echo($usuario["DniUsu"]) ?>" id="inputDni" disabled>
               </div>

            </div>


             <div class="form-row">
               <div class="form-group col-md-3">
                <label for="inputEmail">Correo electrónico</label>
                  <input type="text" class="form-control" name="correo" value="<?php echo($usuario["EmailUsu"]) ?>" id="inputEmail" >
               </div>
               <div class="form-group col-md-3">
                <label for="inputTel">Teléfono</label>
                 <input type="text" class="form-control" name="tlf" value="<?php echo($usuario["TlfUsu"]) ?>" id="inputTel"> 
               </div>
                <div class="form-group col-md-4">
                <label for="inputIban">IBAN</label>
                 <input type="text" class="form-control" name="iban" value="<?php echo($usuario["Iban"]) ?>" id="inputIban"> 
               </div>

            </div>

             <div class="form-row">
               <div class="form-group col-md-3">
                 <label for="inputZona">Zona</label></br>
                  <select name="zona" id="zona">
                     
                      <?php  
                                                                    
                      foreach ($zonas as $indice => $registro) { 
                       
                          if($registro['IdZona'] == $usuario['IdZona']){

                           echo "<option selected value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";
                                            

                        }else{
                         echo "<option value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";
                        }
                      
                      }
                     ?>
                   </select>
              </div>
              <div class="form-group col-md-3">
                  
                     <div style="text-align: center;margin-top: 20px;">
                       <input type="submit" value="Guardar cambios" name="actualizar" class="btn btn-primary btn-lg" onclick="cambios()">
                     </div>
              </div>

          </div>
   
           </br>
   
         </br>
         </br>
  
        </div> 
     </form>
     



      <div class="contenedor2">

          <h3 ><strong>Utiliza nuestros filtros para encontrar al cuidador mas adecuado para ti:</strong> </h3>

           </br>
           </br>

      

        <form class="formu" action="buscar2.php" method="GET">


          <div class="form-row" style="margin-top: 30px;"> 
             
            <div class="form-group col-md-4">
            <label for="inputCuidado"><strong>¿Qué tipo de cuidado buscas para tu mascota? </strong></label></br>

                 <?php
                    
                        foreach ($cuidados as $indice => $registro) {  ?>
                       
                        <label style="color: #000000;">
                       <?php echo '<input type="radio" name="cuidado" required="required" value="'.$registro['IdCuidado'].'">'." " .$registro['NombCuidado'].''; ?>
                       </label></br>
                      <?php  
                       }
                     ?>
            </div>

            
           <div class="form-group col-md-4">
            <label for="inputTipo"><strong>¿Qué tipo de masccota tienes?</strong> </label></br>

                  <?php
                                                    
                         foreach ($mascotas as $indice => $registro) { ?>
                         
                        <label style="color: #000000;">
                       <?php echo '<input type="radio" name="mascota" required="required" value="'.$registro['IdMascota'].'">'. " " . $registro['NombreMascota'].''; ?>
                       </label></br>
                      <?php  
                       }
                     ?>
                     
               
            </div>


            <div class="form-group col-md-4">
                 <label for="inputZona"><strong>Zona</strong></label></br>
                  <select name="zona">
                     <option value="" >Seleccione zona:</option>

                      <?php  
                                                                    
                      foreach ($zonas as $indice => $registro) { 
                       
                          if($registro['IdZona'] == $usuario['IdZona']){

                           echo "<option selected value=". $registro['IdZona'].">". $registro['NombreZona']." </option>";
                                            

                        }else{
                         echo "<option value=". $registro['IdZona'].">". $registro['NombreZona']."</option>";
                        }
                      
                      }
                     ?>

                   </select>

                </div>

         </div>
        
             </br>


          <div class="form-row">

            <div class="form-group col-md-4">
              <label for="inputPrecio"><strong>¿Qué precio se ajusta más a ti?</strong> </label>

               <select name="precio">
                     <option value="3">3€/h</option>
                     <option value="4">4€/h</option> 
                     <option value="6">6€/h</option> 
                     <option value="7">7€/h</option>
                     <option value="8">8€/h</option>
                     <option value="9">9€/h</option> 
                     <option value="10">10€/h</option> 
                     <option value="12">12€/h</option> 
              </select>
             </div>

             
          </div>         
        
               
           
              <button type="submit" class="btn btn-primary btn-lg" style="margin-bottom: 30px;" name="boton4"><i class="fa fa-search fa-lg" aria-hidden="true">  </i>Buscar mi cuidador</button>


        </form> 

      </div>
    </div>
    
                     
            </br>

                 
       </article>

   </section>

 

         
        
     <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram"></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter" ><i class="fab fa-twitter-square"></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>


   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>