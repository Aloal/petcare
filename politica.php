
<?php
require("conexion.php");

?>

     
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
   
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
   <script src=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js"></script>

   <link rel="stylesheet" href="css/estilos.css" />


 <style>
  
 *{
     margin: 0;
     padding: 0;
  
   }

 
 .titulo2{
    align-content: center;
    margin-top: 40px;
    margin-bottom: 30px;
    font-size: 22px;
    text-decoration: underline;

 }

 .btn{
    margin-bottom: 5px;
    
 }

 h3{
   text-decoration: underline;
   font-weight: bold;
 }

  .contenido{
    margin-bottom: 40px;
  }
       
       
</style>

</head>

<body>
 
 <div class="top">
   <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
  </div>
 </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
             <a class="nav-link" href="index.php">Inicio</a>
           </li>

           <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ayuda</a>
                 <div class="dropdown-menu">
                     <a class="dropdown-item" href="ayudaUsu.php">Ayuda Usuarios</a>
                     <a class="dropdown-item" href="ayudaCui.php">Ayuda Cuidadores</a>
                </div>
          </li>             

          <li class="nav-item ml-sm-4">
           <a href="formU.php " class="btn btn-primary">Regístrate</a>
                    
          </li>

          <li class="nav-item ml-sm-4">
            <a href="logU.php"  class="btn btn-success">Mi Área</a>
                    
          </li>

      
         </ul>
       </div>
       
     </nav>


 

   	  <div class="container">

   	    	<div class="titulo2">
              <p><strong>POLÍTICA DE PRIVACIDAD</strong></p>
            </div>

        <div class="contenido">

           <p>Pet CARE te informa sobre su Política de Privacidad respecto del tratamiento y protección de los datos de carácter personal de los usuarios y clientes que puedan ser recabados por la navegación o contratación de servicios a través del sitio Web www.petcare.es.

          En este sentido, el Titular garantiza el cumplimiento de la normativa vigente en materia de protección de datos personales, reflejada en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y de Garantía de Derechos Digitales (LOPD GDD). Cumple también con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas (RGPD).

            El uso de sitio Web implica la aceptación de esta Política de Privacidad así como las condiciones incluidas en el Aviso Legal.</p>

           <p> En el tratamiento de tus datos personales, el Titular aplicará los siguientes principios que se ajustan a las exigencias del nuevo reglamento europeo de protección de datos:</p>

           <p><strong>Principio de licitud, lealtad y transparencia:</strong> El Titular siempre requerirá el consentimiento para el tratamiento de tus datos personales que puede ser para uno o varios fines específicos sobre los que te informará previamente con absoluta transparencia.</p>

           <p><strong>Principio de minimización de datos: </strong> Titular te solicitará solo los datos estrictamente necesarios para el fin o los fines que los solicita.</p>

            <p><strong>Principio de limitación del plazo de conservación:</strong> Los datos se mantendrán durante el tiempo estrictamente necesario para el fin o los fines del tratamiento.</p>

          <p>El Titular te informará del plazo de conservación correspondiente según la finalidad. En el caso de suscripciones, el Titular revisará periódicamente las listas y eliminará aquellos registros inactivos durante un tiempo considerable.</p>

          <p> <strong>Principio de integridad y confidencialidad: </strong> Tus datos serán tratados de tal manera que su seguridad, confidencialidad e integridad esté garantizada. Debes saber que el Titular toma las precauciones necesarias para evitar el acceso no autorizado o uso indebido de los datos de sus usuarios por parte de terceros.</p>

          <p>Para navegar por Pet CARE no es necesario que facilites ningún dato personal. Los casos en los que sí proporcionas tus datos personales son los siguientes:</p>

          <p>-Al contactar a través de los formularios de contacto o enviar un correo electrónico.</p>
          <p>-Al inscribirte en un formulario de suscripción o darte de alta como usuario o cuidador</p>

          <p>El Titular te informa que sobre tus datos personales tienes derecho a:</p>

             <p>-Solicitar el acceso a los datos almacenados.</p>
             <p>-Solicitar una rectificación o la cancelación.</p>
             <p>-Solicitar la limitación de su tratamiento.</p>
             <p>-Al tratamiento</p>
             <p>-Solicitar la portabilidad de tus datos.</p>
            <p>El ejercicio de estos derechos es personal y por tanto debe ser ejercido directamente por el interesado, solicitándolo directamente al Titular, lo que significa que cualquier cliente, suscriptor o colaborador que haya facilitado sus datos en algún momento puede dirigirse al Titular y pedir información sobre los datos que tiene almacenados y cómo los ha obtenido, solicitar la rectificación de los mismos, solicitar la portabilidad de sus datos personales, oponerse al tratamiento, limitar su uso o solicitar la cancelación de esos datos en los ficheros del Titular.</p>

           <p>Para ejercitar tus derechos de acceso, rectificación, cancelación, portabilidad y oposición tienes que enviar un correo electrónico a petcare@gmail.es junto con la prueba válida en derecho como una fotocopia del D.N.I. o equivalente.</p>

           <p>Tienes derecho a la tutela judicial efectiva y a presentar una reclamación ante la autoridad de control, en este caso, la Agencia Española de Protección de Datos, si consideras que el tratamiento de datos personales que te conciernen infringe el Reglamento.</p>

            <p><strong> Tratamiento de datos personales</strong></p>
           <p>Cuando te conectas al sitio Web para mandar un correo al Titular, o realizas una contratación, estás facilitando información de carácter personal de la que el responsable es el Titular. Esta información puede incluir datos de carácter personal como pueden ser tu dirección IP,  dirección de correo electrónico, número de teléfono, y otra información. Al facilitar esta información, das tu consentimiento para que tu información sea recopilada, utilizada, gestionada y almacenada por superadmin.es , sólo como se describe en el Aviso Legal y en la presente Política de Privacidad.</p>

           <p>Para proteger tus datos personales, el Titular toma todas las precauciones razonables y sigue las mejores prácticas de la industria para evitar su pérdida, mal uso, acceso indebido, divulgación, alteración o destrucción de los mismos.</p>

            <p>El sitio Web está alojado en "Lucushost.com". La seguridad de tus datos está garantizada, ya que toman todas las medidas de seguridad necesarias para ello. Puedes consultar su política de privacidad para tener más información.</p>

           <p>Exactitud y veracidad de los datos personales</p>
           <p>Te comprometes a que los datos facilitados al Titular sean correctos, completos, exactos y vigentes, así como a mantenerlos debidamente actualizados.</p>

          <p>Como Usuario del sitio Web eres el único responsable de la veracidad y corrección de los datos que remitas al sitio exonerando a el Titular de cualquier responsabilidad al respecto.</p>

           <p><strong>Aceptación y consentimiento</strong></p>
          <p>Como Usuario del sitio Web declaras haber sido informado de las condiciones sobre protección de datos de carácter personal, aceptas y consientes el tratamiento de los mismos por parte de el Titular en la forma y para las finalidades indicadas en esta Política de Privacidad.</p> 

          <p><strong>Revocabilidad</strong></p>
          <p>Para ejercitar tus derechos de acceso, rectificación, cancelación, portabilidad y oposición tienes que enviar un correo electrónico a petcare@gmail.es junto con la prueba válida en derecho como una fotocopia del D.N.I. o equivalente.</p>

           <p>El ejercicio de tus derechos no incluye ningún dato que el Titular esté obligado a conservar con fines administrativos, legales o de seguridad.</p>

           <p><strong>
           Cambios en la Política de privacidad </strong></p>
           <p> Titular se reserva el derecho a modificar la presente Política de Privacidad para adaptarla a novedades legislativas o jurisprudenciales, así como a prácticas de la industria.</p>

          <p>Estas políticas estarán vigentes hasta que sean modificadas por otras debidamente publicadas.</p></br>

           <h3>TÉRMINOS Y CONDICIONES</h3></br>

                    
           <h5><strong>Productos y servicios:</strong></h5>

           <p>PET Care somos una web con sede en Asturias que presta servicios de cuidado de mascotas dentro de la región (CuidaMascota S.L).</p>
           <p> El servicio es prestado a usuarios que demandan la cobertura de esta actividad. Para ello deberán darse de alta como Usuarios registrados para tener acceso al servicio y tendrán libertad para contactar con los cuidadores que consideren enviando peticiones de contratación a la espera de que el cuidador las acepte.</p><p> De otra parte, figuran los cuidadores de mascotas. Si se desea formar parte de la base de datos de cuidadores de PET Care, éstos deberán darse de alta como cuidadores en la aplicación. Los cuidadores deberán aceptar o rechazar las peticiones de contrato que vayan recibiendo.</p>
           <p>Tanto los Usuarios registrados como los cuidadores se comprometen a usar la aplicación de manera lícita y sometiéndose a las normas que puedan establecerse desde PET Care. Cualquier tipo de relación comercial establecida fuera de los cauces de la aplicación podrá se objeto de denuncia expresa por parte de la sociedad "CuidaMascota S.L."

           <h5><strong>Precios y pagos:</strong></h5>


             <p>Los Usuarios registrados estarán sujetos a una cuota mensual de 10 €, que sérá cobrada por cuanta bancaria. Además cuando soliciten un servicio sabrán de antemano cuanto les costará. Una vez prestado el servicio, este será cargado en cuenta bancaria dentro de los 10 días siguientes a la finalización del mismo</p> 
          
             <p>En el caso de los cuidadores, los precios del servicio serán establecidos por PET Care. Todo cuidador que quiera entrar a formar parte de la comunidad de cuidadores deberá darse de alta sin estar sujeto por ello a ninguna cuota mensual. Además también deberá  someterse a los rangos de precios establecidos de antemano para los servicios. Cada cuidador es libre de escoger el precio que considere que valen sus servicios siempre dentro de los rangos.</p>
             <p>Los precios que figuran son por hora de servicio, el cuidador cobrará el precio/hora que haya escogido por el número de horas de servicio; sobre este total PET Care aplicará una comisión en conceptos de gastos de gestión del 3%. El cobro de los servicios serán dentro de los 15 días siguientes a la finalización del mismo a tráves de cuenta bancaria.</p>
             <p>Pet Care se reserva el derecho de cambiar esta política de precios, pagos y cobros en cualquier momento previo aviso a los usuarios de la aplicación.</p>

            <h5><strong>Cese del servicio:</strong></h5>

            <p>Tanto los Usuarios registrados como los cuidadores podrán cesar su relación con la web en cualquier momento. Para ello simplemente deberán hacerlo a través de su área personal mediante la opción "Baja Usuario" - "Baja Cuidador". Asi mismo será necesario comunicación  por medio de correo electrónico a <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#0000FF">petcare@gmail.com </a>, indicando Nick y contraseña asi como el motivo por el cual ha cursado su baja. En el caso de los Usuarios registrados la cuota mensual del mes en curso será cargada en su totalidad por cuenta bancaria. Si quedasen cantidades pendientes de liquidar a cuidadores, éstas seguirán el proceso normal establecido, de cobro dentro de los 15 dias siguientes a la finalización del servicio.</p>
            <p>Así mismo CuidaMascota S.L se reserva el derecho para dejar de contar con cuidadores o usuarios registrados cuyo comportamiento ponga en evidencia la prestación de servicios fuera de los cauces normales y la vulneración de las normas de la aplicación Pet Care. </p>



            <h5><strong>Cambios en las condiciones</strong></h5>

            <p>Pet Care se reseva el derecho de realizar cambios en cualquier de los puntos de estos Términos y condiciones descritas.</p>

           <div class="irarriba">
             <a href="#"><strong><i class="fas fa-arrow-up"></i>  Subir</strong></a> 
             </div> 

      </div>

    </div>

    <div class="btn" style="color:black; margin-bottom: 8px;">
       <a href="javascript: history.go(-1)"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver</strong></a> 
      </div>

         
         
    <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram" > <i class="fab fa-instagram " ></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square " ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>

</html>