
<!DOCTYPE html>
<html lang="es">
<head>

  <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

   <link rel="stylesheet" href="css/estilos.css">


  <style type="text/css">
   
  
article p{
   margin-right: 100px;
   margin-left: 100px;
}

 article h5{
  color:#009999;
 margin-left: 90px; 
 margin-bottom: 15px;
 margin-top: 15px;
 }

  
.form {
  width: 100%;
  max-width: 600px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  margin-top: 15px;
  margin-bottom: 15px;
}

#cuidador{
  position: center;
  margin-top: 40px;
  margin-bottom: 15px;
}

.btn{
    margin-bottom: 5px;
    
 }

section{
   margin-top: 25px;
  margin-bottom: 25px;
  margin-left: 25px;
}


  </style>

</head>


<body>
   
   
   <div class="top">
    <div class="container-fluid pt-1 my-3 text-black m-2" >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="200" height="200">
         <h1><strong>PET Care</strong></h1> <br/>  
         <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el cuidador ideal para tu perro o gato </h4>
      </div>
   </div>
  </div>
   
     <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
           <li class="nav-item active">
              <a class="nav-link" href="index.php">Inicio</a>
           </li>

           
          <li class="nav-item ml-sm-4">
           <a href="formU.php " class="btn btn-primary">Regístrate</a>
                    
          </li>

          <li class="nav-item ml-sm-4">
            <a href="logU.php"  class="btn btn-success">Mi Área</a>
                    
          </li>

      
         </ul>
       </div>
       
     </nav>
    

      <section class="main row pt-2 mr-1" >

          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ml-2">

           <h5><strong>¿Cómo puedo encontrar a los cuidadores en PET CARE?</strong></h5>
           <p class="uno">Muy fácil: usa la herramienta de búsqueda en nuestra página de inicio para encontrar cuidadores, ahí podrás ver los cuidadores disponibles. Este proceso es totalmente grautito.
            Para contactar con nuestros cuidadores deberás de crear  una cuenta de usuario haciendo clic en <a href="formU.php"><strong> REGÍSTRATE</strong></a>  así ya  puedes empezar a contactar con cuidadores mediante el pago de un cuota mensual de  <strong>¡sólo 10€/mes!</strong></p>

           <h5><strong>¿Puedo ponerme en contacto con varios cuidadores a la vez?</strong></h5>
           <p class="dos">Sí. El sistema de mensajería de PET Care te permite enviar notificaciones a varios cuidadores de mascotas, podrás cancelar tus peticiones, realizar otra búsqueda, seleccionar y realizar un nueva petición a otro cuidador.Los cuidadores que estén interesados ​​en tu solicitud te responderán lo antes posible a través del sistema de mensajería de PET Care. Serán los cuidadores los que acepten tu solicitud. Si por cualquier motivo tienes que cancelar un contrato que un cuidador te hubiese aceptado, puedes hacerlo hasta 48 hora antes del mismo. El dia anterior al servicio el cuidador ya se pondrá en contacto contigo</p>

           
           <h5><strong>¿Cuánto tardan en contestar los cuidadores?</strong></h5>
           <p class="tres">No te preocupes si no recibes una confirmación inmediata tras realizar tu búsqueda. Nuestros cuidadores de mascotas se conectan a sus cuentas con regularidad y te responderán en un plazo máximo de 24 horas. Por regla general, las primeras respuestas te llegarán pocas horas después de tu solicitud. Si al cabo de 48 horas aún no has recibido una respuesta o tu petición ha sido rechazada, es muy posible que el cuidador ya tenga comprometida es fecha, de modo que intenta añadir más cuidadores y peticiones de contrato.</p>
             

            <h5><strong>¿Cuánto y cómo debo pagarle a mi cuidador?</strong></h5>
           <p class="cuatro"> En PET CARE disponemos de amplia gama de precios y creemos que es justo que nuestros cuidadores decidan el precio que quieren cobrar dentro de un amplio rango de precios que hemos establecido. El precio de cada cuidador se muestra directamente en su perfil para que siempre sepas el coste de sus servicios de antemano. El precio que aparece durante la búsqueda corresponde al coste por hora. Aqui podrían darse 3 posibles escenarios:</p>
           <p> a) Duración del servicio menor a 24 horas: el precio total serán el numero de horas por el precio/hora</p>
           <p> b) Duración del servicio entre 24 y 96 horas: el precio total serán el numero de horas por el precio/hora aplicando un descuento del 40% en el precio/hora.</p>
           <p> c) Duración del servicio mas allá de las 96 horas: el precio total serán el numero de horas por el precio/hora aplicando un  descuento del 60% en el precio/hora.</p>
           <p>De todos modos a la hora de realizar peticiones de contratos tendrás la opción de saber exactemente a cuenta asciende el servicio que solicitas.

            El coste del servicio se girará por tu cuenta bancaria en los 10 días siguientes al término del servicio.</p>
             
             <h5><strong>No estoy satisfecho con el primer cuidador que he reservado,¿Puedo buscar otro?</strong></h5>
           <p class="cinco">Sí, por supuesto. Puedes hacer una nueva búsqueda y encontrar otro cuidador si el primero no ha cumplido con tus expectativas. Para cambiar tu cuidador, accede a tu cuenta, y  realiza una nueva búsqueda según tus preferencias. Nuestro sistema te permite enviar una notificación a los mismos cuidadores de mascotas que antes y/o seleccionar otros nuevos. Del mismo modo, una vez haya sido prestado el servicio, también dispondrás de la opcion de "opiniones", donde podrás expresarte con total libertad sobre como ha ido un servicio con un cuidador en concreto, dichas opiniones serán visibles para otros usuarios a modo de guía a lo hora de contactar con cuidadores.</p>

            <h5><strong>¿Qué debería darle al cuidador el dia que se quede a cargo de mi mascota?</strong></h5>
           <p class="seis">
            Te recomendamos que le dejes todo lo esencial: correa, comida, juguetes, cesta y toda la información médica necesaria, sobre todo si el servicio es de acampoñamiento al veterinario. También te aconsejamos que informes a tu cuidador con anticipación sobre cualquier requisito adicional de tu mascota (por ejemplo, si tu mascota sigue un régimen especial o si está tomando algún medicamento).</p>

            <h5><strong>¿Tiene PET CARE un servicio de accidentes que cubra el cuidado de las mascotas?</strong></h5>
           <p class="siete">Sí. ¡Nuestra garantía PetSafe está incluida en todas las reservas. Nuestra garantía PetSefe cubre los gastos veterinarios en los que incurra tu mascota en caso de accidente durante su estancia con un cuidador de mascotas, hasta 1000€ en gastos de emergencia.</p>


            <h5><strong>¿Cómo puedo ponerme en contacto con Pet Care?</strong></h5>
           <p class="siete"> Para cualquier problema que surja como pérdida de contraseñas, peticiones de reestablecimiento de las mismas, quejas, etc....,  puedes ponerte en contacto con nosotros por medio de nuestro corre electrónico <strong> <a href="mailto:petcare@gmail.com" style="text-decoration: none; color:black">petcare@gmail.com  <i class="far fa-envelope"></i> </a></strong>, identificándote y explicádonos cualquier situación.</p>
                 

           </br>
              <div class="btn" style="color:black; margin-bottom: 8px;">
               

               <a href="javascript: history.go(-1)"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver</strong></a> 
              
              </div>


       </article>

   </section>

  
 
      <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook"> <i class="fab fa-facebook-square"></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram"> <i class="fab fa-instagram"></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square" ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>


   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>