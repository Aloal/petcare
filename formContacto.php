
<?php

  require("conexion.php");
  require("libreria/funciones.php");
  require ("libreria/regbdContrato.php");
 
  session_start();

     $usuario=getUsuario_por_nombre($_SESSION['usuario']);
     $cuidador=getCuidador_por_id($_GET['Idcui']);

        if (isset($_POST['peticion'])) {

            registroContrato();
           
            } 

      
    ?>


<!DOCTYPE html>
<html lang="es">
<head>


  <meta charset="utf-8">

     
      <script src="js/jquery.js"></script>
      <script src="js/funciones.js"></script>


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

     <link rel="stylesheet" href="css/estilos.css" />
   

<style type="text/css">


.row{
  display: flex;
  justify-content: center;
  align-items: center;
  
}


.coste{
  border: 2px solid black;
  border-radius: 10px 10px 10px 10px;
  padding: 15px;
   max-width: 400px;
   margin-left: 10px;
     
  
}


.calendario h4{
 margin-bottom: 60px;
 margin-top: 25px;
 font-family: verdana;
 color:#009999;

}

.calendario{
  margin-bottom: 40px;
}

 label{
  margin-left: 18px;

 }


.calendario p{
  display: inline-block;
  max-width: auto;
 }

 .formu{
 margin-top: 50px;

 }

 .btn{
    margin-bottom: 5px;
    
 }


 .btn1{
  
   margin-bottom: 8px;
   margin-top: 20px;
   margin-left: 12px;
 }

  .comment {
    float: left;
   width: 100%;
    height: auto;
    
}

 

  .textinput {
      float: left;
     width: 75%;
     min-height: 120px;
     outline: none;
      resize: none;
      border: 1px solid grey;
      border-radius: 10px;
      box-shadow: 10px 10px 14px 2px rgba(0,0,0,0.47);
      padding: 10px;
     }



 </style>

  </head>

  <body>

  
      <div class="top">
        <div class="container-fluid pt-4 pb-2  text-black "  >
     <div class="titulo">
        <img src="imagenes_video/logo.png" alt="logo" class="rounded-circle visible only on d-none d-sm-inline" width="225" height="225">
         <h1 style=" "><strong>PET Care</strong></h1> <br/>
            <h4 class= "text-md-center font-weight-bolder visible only on d-none d-lg-block" >En Pet Care  podrás encontrar el <strong>cuidador</strong> ideal para tu perro o gato </h4>
      </div>
   </div>
  </div>

    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark">

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
      </button>

          <div class="collapse navbar-collapse" id="collapsibleNavbar">
      
        <ul class="navbar-nav ">
          <li class="nav-item active">
             <a class="nav-link" href="index.php" onclick="sesion();">Inicio</a>
           </li>

                     
          <li class="nav-item ml-sm-4">
            <a href="cerrar_sesionU.php"  class="btn btn-primary" role="button">Cerrar sesión</a>
            
          </li>

      
         </ul>
       </div>
       
     </nav>

   
                     <script>
    
                       function calcular_precio(){
                        
                           var inicio = document.getElementById("inicio").value;
                           var fin = document.getElementById("fin").value;
                           
                            var fechainicio = new Date(inicio); 
                            var fechafin = new Date(fin);

                            var diferencia=(fechafin.getTime() - fechainicio.getTime() );

                            if(diferencia<0){
                              
                              alert("La fecha fin ha de ser mayor que la fecha inicio");

                              return;

                            } 

                            var horas= diferencia/3600000;


                             if (horas<=24){
                           
                                var costeFinal= horas*<?php echo($cuidador['PrecioH']) ?>;

                                var coste= costeFinal.toFixed(2);
                                 
                                document.getElementById("precio").value = coste +" " + "€";

                                 }                              
                               

                            else if (horas<=96) {

                              var costeFinal=  (horas*<?php echo($cuidador['PrecioH']) ?>)*60/100;

                              var coste= costeFinal.toFixed(2);
                                 
                                document.getElementById("precio").value = coste +" " + "€";
                                                              
                              }
                              else  {
                                
                                var costeFinal=  (horas*<?php echo($cuidador['PrecioH']) ?>)*40/100;
                               
                                var coste= costeFinal.toFixed(2);
                                 
                                document.getElementById("precio").value = coste +" "  + "€";
                                                                 
                                }
                          
                         
                        }
                                                                     
                      </script>

      

            <div class="row mr-2" >
              

                <div class="calendario col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">
        

                 <div class="container">

                   <h4><strong>Rellena las fechas y horas del servicio en el siguiente formulario</strong></h4>
                     
                   <form name="formupeticion" method="POST">
                         

                     <p><strong> Usuario:</strong></p>    <input type="text" size="6"  disabled="disabled" style="margin-right: 35px;  box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.35); background-color:#FFFFE0 " value="<?php echo($usuario["NickUsu"]) ?>">

                     <input type="text" size="6" hidden="hidden"  name="usuario" style="margin-right: 35px; " value="<?php echo($usuario["IdUsu"]) ?>">

                    

                    <p><strong> Cuidador:</strong></p>   <input type="text" size="6" disabled="disabled" style="margin-right: 35px; box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.35); background-color:#FFFFE0" value="<?php echo($cuidador['NickCui']) ?>">

                     <input type="text" size="6" hidden="hidden"  name="cuidador" style="margin-right: 35px; " value="<?php echo($_GET["Idcui"]) ?>">

                    
                         </br>
                         </br>
                         </br>

                    <div class="horas col-xs-8 col-sm-8 col-md-8 col-lg-12 col-xl-11">

                     <p><strong>  Fecha y hora inicio:</strong></p>
                      <input type="datetime-local" name="fechainicio" id="inicio" required="required" value="2022-01" autofocus="autofocus" autocomplete="on"  min="2021-01-01T08:30" max="2025-12-31T23:00" step="1" />
                              
                     <p><strong>  Fecha y hora fin:</strong></p>
                       <input type="datetime-local" name="fechafin" id="fin" required="required" value="2022-01" autofocus="autofocus" autocomplete="on" min="2021-01-01T08:30" max="2025-12-31T23:00" step="1" /></br>
                        </br>
                        </br>
                   </div>

                    <div class="mianuncio"><p><strong>Comentarios:</strong></p>
                     <div class="comment">
                       <textarea class="textinput" cols="40" rows="4" id="coment" name="comentario"> [Soy <?php echo($usuario["NickUsu"]) ?> mi teléfono de contacto es: <?php echo($usuario["TlfUsu"]) ?> ]----> </textarea>
                     </div>
                    </div>  
                                          
                      

                       <input type="submit" name="peticion" style="margin-top: 35px; margin-bottom: 35px;"  value="Enviar petición" class="btn btn-primary" onclick="enviar()">


        
                   </form>

                    <p><strong>Si ya lo tienes claro puedes enviar tu petición, deberás esperar a que el cuidador acepte tu solicitud. Cuando la solicitud haya sido aceptada será el cuidador quien te contacte 1 día antes del servicio.</strong> </p>
                       
                 </div>


                  
                  
              </div>

          
                  <div class="coste col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2 ">


                     <p><strong>Una vez hayas escogido las fechas y horas de inicio y fin del servicio, y antes de enviar tu petición, puedes obtener aquí el coste total que supondrá el cuidado. </strong> </p>

              
                     <input type="button" name="calcular" style="margin-top: 25px; margin-bottom: 25px;"  value="Cacular coste" class="btn btn-success" onclick="calcular_precio()">

                       <label for="nombre">Precio final: 
                       <input type="text" size="6" id="precio" name="precio" value=""/></label>


                </div>
                

            </div>
        

          <div class="btn" style="color: #B266FF;">
            <a href="javascript: history.go(-2)"><strong><i class="fa fa-arrow-left" aria-hidden="true">  </i>Volver a la búsqueda</strong></a> 
          </div>    

      
  

   <footer class="bg-secondary text-white text-center text-lg-start">
   <div class="container pt-1">
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-4 col-sm-12 mb-4 mb-md-0">
        <h6 class="text-uppercase mt-4">Contacto</h6>

        <p>C/ Uría, Nº 30, bajo<br/>
          Oviedo-Asturias<br/>
          <i class="fas fa-phone" _mstvisible="2"></i> 985998877 - 699999999<br/>
           <a href="mailto:petcare@gmail.com" style=" text-decoration: none; color:#FAEBD7">petcare@gmail.com  <i class="far fa-envelope"></i> </a>
                     
        </p>
      </div>
      <!--Grid column-->
    

        <div class="col-lg-5 col-md-5 col-sm-12 mb-3 mb-md-0">
        
           <div class="text-center p-6 mt-4">

             <a href="html/quienessomos.html" style="color:#FFFFFF; text-decoration: none; font-size: 18px; margin-bottom: 15px;">Quienes somos</a><br/>
                             
                <a href="politica.php" style="color:#FFFFFF; text-decoration: none;">Politica de Privacidad</a></br>
                
                <a href="avisolegal.php" style=" color:#FFFFFF; text-decoration: none; ">Aviso Legal y Cookies </a></br>   © 2020 Copyright:<a class="text-dark" href="#"> albertolopal.com</a>
          </div>
         </div>

       
         <div class="col-lg-3 col-md-3 col-sm-12 mb-3 mb-md-0">

          <div class="redes">

             <p style="margin-top: 20px; ">Síguenos en RRSS</p> 

             <p>
               <a href="https://www.facebook.com/alberto.lopezalvarez" data-toggle="tooltip" title="Facebook" > <i class="fab fa-facebook-square " ></i></a>

               <a href="https://www.instagram.com/bestfriendsanimalsociety/" data-toggle="tooltip" title="Instagram"> <i class="fab fa-instagram " ></i></a>
               
               <a href="https://twitter.com/mundoAnimalia" data-toggle="tooltip" title="twitter"><i class="fab fa-twitter-square" ></i></a>

            </p>
          </div>
         </div>
 
     </div>
    </div>
  </footer>
   
  

   <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
   <script src="js/bootstrap-datetimepicker.min.js"></script>

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>

</html>